<?php
defined('BASEPATH') or exit('no direct sript access allowed');

class User_model extends CI_Model
{
  public function create($data)
  {
    $this->db->insert('users', $data);
  }

  function all()
  {
    $users = $this->db->get('users')->result_array();
    return $users;
  }
  public function getUser($userid)
  {
    $this->db->where('user_id', $userid);
    $user = $this->db->get('users')->row_array();
    return $user;
  }
  public function updateUser($userid, $data)
  {
    $this->db->where('user_id', $userid);
    $this->db->update('users', $data);
  }
  public function delete($userid)
  {
    $this->db->where('user_id', $userid);
    $this->db->delete('users');
  }
}
