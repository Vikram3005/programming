<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>List Users</title>
    <link rel="stylesheet" href="<?php echo base_url() . 'application/asset/js/bootstrap.min.js'; ?>">
    <link rel="stylesheet" href="<?php echo base_url() . 'application/asset/css/bootstrap.min.css'; ?>">
</head>

<body>
    <div class="navbar-dark bg-dark">
        <div class="container">
            <a href="#" class="navbar-brand">CRUD OPERATION</a>
        </div>
    </div>
    <div class="container" style="padding-top: 10px;">
        <div class="row">
            <div class="col-md-12">
                <?php $success = $this->session->userdata('success');
                if (!$success == "") { ?>
                    <div class="alert alert-success"><?php echo $success; ?></div>
                <?php } ?>
                <?php $failure = $this->session->userdata('failure');
                if ($failure != "") {
                ?>
                    <div class="alert alert-success"><?php echo $failure;  ?></div>
                <?php } ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-6">
                        <h3>List Users</h3>
                    </div>
                    <div class="col-6 text-right">
                        <a href="<?php echo base_url() . 'index.php/users/create' ?>" class="btn btn-primary">Create/Add</a>
                    </div>
                </div>
            </div>
        </div>

        <hr>
        <div class="row">
            <div class="col-md-12">
                <table class="table table-striped">
                    <tr>
                        <th>ID</th>
                        <th>NAME</th>
                        <th>EMAIL</th>
                        <th>CREATED_DATE</th>
                        <th>PHONE</th>
                        <th width="60">EDIT</th>
                        <th width="100">DELETE</th>
                    </tr>
                    <?php if (!empty($users)) {
                        foreach ($users as $user) { ?>
                            <tr>
                                <td><?php echo $user['user_id'] ?></td>
                                <td><?php echo $user['uname'] ?></td>
                                <td><?php echo $user['email'] ?></td>
                                <td><?php echo $user['created_date'] ?></td>
                                <td><?php echo $user['phone'] ?></td>
                                <td> <a href="<?php echo base_url() . 'index.php/users/edit/' . $user['user_id'] ?>" class="btn btn-primary">Edit</a></td>
                                <td> <a href="<?php echo base_url() . 'index.php/users/delete/' . $user['user_id'] ?>" class="btn btn-danger">Delete</a></td>
                            </tr>
                        <?php }
                    } else {
                        ?>
                        <tr>
                            <td colspan="5">Records not found.</td>
                        </tr>
                    <?php  } ?>
                </table>
            </div>
        </div>
    </div>
</body>

</html>