<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Edit</title>
    <link rel="stylesheet" href="<?php echo base_url() . 'application/asset/js/bootstrap.min.js'; ?>">
    <link rel="stylesheet" href="<?php echo base_url() . 'application/asset/css/bootstrap.min.css'; ?>">
</head>

<body>
    <div class="navbar-dark bg-dark">
        <div class="container">
            <a href="#" class="navbar-brand">CRUD OPERATION</a>
        </div>
    </div>
    <div class="container" style="padding-top: 10px;">
        <h3>Edit User</h3>
        <hr>
        <form name="update user" action="<?php base_url() . 'index.php/users/edit' . $user['user_id']; ?>" method="post">
            <div class="row">
                <div class="col-md-5">
                    <div class="form-group">
                        <label>Username</label>
                        <input type="text" name="username" id="username" value="<?php echo set_value('username', $user['uname']); ?>" class="form-control">
                        <?php echo form_error('username'); 
                        ?>
                    </div>
                    <div class="form-group">
                        <label>Email</label>
                        <input type="email" name="email" id="email" value="<?php echo set_value('email', $user['email']); ?>" class="form-control">
                        <?php echo form_error('email'); 
                        ?>
                    </div>
                    <div class="form-group">
                        <label>Password</label>
                        <input type="password" name="password" id="password" value="<?php echo set_value('password', $user['password']); ?>" class="form-control">
                        <?php echo form_error('password'); 
                        ?>
                    </div>
                    <div class="form-group">
                        <label>Created_date</label>
                        <input type="date" name="created_date" id="created_date" value="<?php echo set_value('created_date', $user['created_date']); ?>" class="form-control">
                        <?php echo form_error('created_date'); 
                        ?>
                    </div>
                    <div class="form-group">
                        <label>Phone</label>
                        <input type="text" name="phone" id="phone" value="<?php echo set_value('phone', $user['phone']); ?>" class="form-control">
                        <?php echo form_error('phone'); 
                        ?>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-primary">Update</button>
                        <a href="<?php echo base_url() . 'index.php/users/index'; ?>" class="btn btn-secondary">Cancel</a>
                    </div>
                </div>
            </div>
        </form>
    </div>
</body>

</html>