<?php
defined('BASEPATH') OR exit('no direct script access allowed.');


class Users extends CI_Controller 
{
   

    public function __construct()
    {
        parent::__construct();
        $this->load->model('User_model');
    }
  
    public function index()
    {
        $users = $this->User_model->all();
        $data = array();
        $data['users'] = $users;
        $this->load->view('list',$data);
    }
    public function create()
    {
        $this->form_validation->set_rules('username','Username','required');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'required');
        $this->form_validation->set_rules('created_date', 'Created_date', 'required');
        $this->form_validation->set_rules('phone', 'Phone', 'required');
       
        if ($this->form_validation->run() == FALSE) {
            $this->load->view('adduser');
        } else {
            $data = array(
                'uname' =>$this->input->post('username'),
                'email' => $this->input->post('email'),
                'password' => $this->input->post('password'),
                'created_date' => $this->input->post('created_date'),
                'phone' => $this->input->post('phone')
                
            );
            $this->User_model->create($data);
            $this->session->set_flashdata('success','Record added successfully!');
            redirect(base_url().'index.php/users/index');
        }  
    }
    public function edit($userid)
    {
        $user = $this->User_model->getUser($userid);
        $data = array();
        $data['user'] = $user;

        $this->form_validation->set_rules('username', 'Username', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'required');
        $this->form_validation->set_rules('created_date', 'Created_date', 'required');
        $this->form_validation->set_rules('phone', 'Phone', 'required');
        
        if ($this->form_validation->run() == FALSE) {
            $this->load->view('edit', $data);
        }else
        {
            $data = array();
            $data['uname'] = $this->input->post('username');
            $data['email'] = $this->input->post('email');
            $data['password'] = $this->input->post('password');
            $data['created_date'] = $this->input->post('created_date');
            $data['phone'] = $this->input->post('phone');
            
            $this->User_model->updateUser($userid,$data);
            $this->session->set_flashdata('success','Record Updated Successfully');

            redirect(base_url().'index.php/users/index');
        }    
    }
    public function delete($userid)
    {
        $user = $this->User_model->getUser($userid);
    
        if (empty($user)) {
            $this->session->set_flashdata('failure', 'Record not available in database');
            redirect(base_url().'index.php/users/index');
        }
        else
        {
            $this->User_model->delete($userid);
            $this->session->set_flashdata('success', 'Record deleted Successfully');
            redirect(base_url() . 'index.php/users/index');
        }
    }


}




?>