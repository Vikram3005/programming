<?php

function SwapNumbers($num1,$num2)
{
    $temp = $num1;
    $num1 = $num2;
    $num2 = $temp;

    echo "After swap num1:".$num1 ." and num2 :".$num2."\n";
}

$a = 100;
$b = 200;
echo "Before swap num1:" . $a . " and num2 :" . $b . "\n";
SwapNumbers($a,$b);