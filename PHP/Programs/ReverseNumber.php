<?php

function ReverseNumber($num)
{
    $newno = $num;
    $sum = 0;
    while ($newno != 0) {

        $rem = $newno % 10;
        $sum = $sum * 10   + $rem;
        $newno = $newno / 10;
    }
    //echo "Reverse of number" . $num . " is " . $sum ."\n";
    return $sum;
}

$rev  = ReverseNumber(12123);
echo $rev;