<?php

function Isprime($num)
{
    $flag = 0;

    for ($i = 2; $i <= $num / 2; $i++) {

        if ($num % $i == 0) {
            $flag = 1;
            break;
        }
    }
    return $flag;
}


$status = Isprime(17);
if ($status == 1) {
    echo "Not a Prime Number.\n";
} else {
    echo "Is Prime Number.\n";
}
