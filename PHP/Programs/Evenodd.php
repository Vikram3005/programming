<?php

function EvenOdd($num)
{
    if ($num % 2 == 0) {
        return true;
    } else {
        return false;
    }
}

$status = EvenOdd(229);
if ($status) {
    echo "It is Even Number.\n";
} else {
    echo "It is Odd Number.\n";
}

