<?php

function AmstrongNumber($num)
{
    $newnum = $num;
    $sum = 0;

    while ($newnum != 0) {
        $rem = $newnum % 10;
        $sum = $sum + $rem * $rem * $rem;
        $newnum = $newnum / 10;
    }
    if ($sum == $num) {
        echo $num . " is a Amstrong Number.\n";
    } else {
        echo $num . " is Not a Amstrong Number.\n";
    }
}

AmstrongNumber(150);
