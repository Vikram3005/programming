<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Add</title>
    <link rel="stylesheet" href="<?php echo base_url() . 'application/asset/js/bootstrap.min.js'; ?>">
    <link rel="stylesheet" href="<?php echo base_url() . 'application/asset/css/bootstrap.min.css'; ?>">
</head>

<body>
    <div class="navbar-dark bg-dark">
        <div class="container">
            <a href="#" class="navbar-brand">Sign Up</a>
        </div>
    </div>
    <div class="container" style="padding-top: 10px;">
        <h3>Create User</h3>
        <hr>
        <?php if ($this->session->flashdata('success')) { ?>
            <div class="alert alert-success  col-5"><?php echo $this->session->flashdata('success') ?></div>
        <?php } ?>
        <form name="create user" action="<?php base_url() . 'index.php/users/signup'; ?>" method="post">
            <div class="row">
                <div class="col-md-5">
                    <div class="form-group">
                        <label>Username</label>
                        <input type="text" name="username" id="username" value="<?php echo set_value('username') ?>" class="form-control">
                        <?php echo form_error('username'); ?>
                    </div>
                    <div class="form-group">
                        <label>Email</label>
                        <input type="email" name="email" id="email" value="<?php echo set_value('email') ?>" class="form-control">
                        <?php echo form_error('email'); ?>
                    </div>
                    <div class="form-group">
                        <label>Password</label>
                        <input type="password" name="password" id="password" value="<?php echo set_value('password') ?>" class="form-control">
                        <?php echo form_error('password'); ?>
                    </div>
                    <div class="form-group">
                        <label>Created_date</label>
                        <input type="date" name="created_date" id="created_date" class="form-control">
                        <?php echo form_error('created_date'); ?>
                    </div>
                    <div class="form-group">
                        <label>Phone</label>
                        <input type="text" name="phone" id="phone" class="form-control">
                        <?php echo form_error('phone'); ?>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-primary">Create</button>
                        <a href="<?php echo base_url() . 'index.php/users/signin'; ?>" class="btn btn-secondary">Cancel</a>
                    </div>
                </div>
            </div>
        </form>
    </div>
</body>

</html>