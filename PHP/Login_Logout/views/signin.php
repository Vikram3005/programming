<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Add</title>
    <link rel="stylesheet" href="<?php echo base_url() . 'application/asset/js/bootstrap.min.js'; ?>">
    <link rel="stylesheet" href="<?php echo base_url() . 'application/asset/css/bootstrap.min.css'; ?>">
</head>
<body>
    <div class="row justify-content-center">
        <div class="col-6">
            <h1><?php echo $title ?></h1> 
            <?php if ($this->session->flashdata('msg')) { ?>
                <div class="alert alert-danger col-5"><?php echo $this->session->flashdata('msg') ?></div>
            <?php }?>
            <form name="create user" action="<?php base_url() . 'index.php/users/signin'; ?>" method="post">
                <div class="row">
                    <div class="col-md-5">
                        <div class="form-group">
                            <label>Email</label>
                            <input type="email" name="email" id="email" placeholder="email@gmail.com" class="form-control">
                            <?php echo form_error('email'); ?>
                        </div>
                        <div class="form-group">
                            <label>Password</label>
                            <input type="password" name="password" placeholder="Password" id="password" class="form-control">
                            <?php echo form_error('password'); ?>
                        </div>
                        <div class="form-group">
                            <input type="submit" class="btn btn-primary" value="Login" />
                            <a href="<?php echo base_url() . 'index.php/users/signup'; ?>" class="btn btn-secondary">Sign-Up</a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</body>

</html>