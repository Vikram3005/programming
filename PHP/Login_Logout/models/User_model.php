<?php 
defined('BASEPATH') OR exit ('no direct script access allowed');

class User_model extends CI_Model 
{
    public function save($data)
    {
        $this->db->insert('users',$data);
    }
    
    public function login($email,$password)
    {
        $this->db->where('email',$email);
        $this->db->where('password',$password);
        $query = $this->db->get('users');


        if ($query->num_rows() == 1) {
            return $query->row();
        }

        return false;
    }
}
?>