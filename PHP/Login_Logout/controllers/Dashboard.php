<?php  
defined('BASEPATH') OR exit('no direct access allowed');

class Dashboard extends CI_Controller       
{
    public function __construct()
    {
        parent::__construct();
        $this->logged_in();
    }
  private function logged_in()
  {
      if (!$this->session->userdata('authenticated')) {
            redirect('users/signin');
      }
  }

    public function index()
    {
        $data['title'] = "Dashboard";

        $this->load->view('index',$data);
      
            
    }
}
