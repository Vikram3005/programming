<?php  
defined('BASEPATH') OR exit('no direct access allowed');

class Users extends CI_Controller       
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('User_model') ;
    }
    public function logout()
    {
        $this->session->sess_destroy();
        redirect('users/signin');
    }

    public function signin()
    {
        if ($this->session->userdata('authenticated')) {
            redirect('dashboard');
        }
        $data['title'] = "Login";

        $this->form_validation->set_rules('email','Email','required|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'required');

        if ($this->form_validation->run() == FALSE) {
            $this->load->view('signin',$data);
        }else{
            $email = $this->input->post('email');
            $password = $this->input->post('password');

            $user = $this->User_model->login($email,$password);
            
            if ($user) {
               $userdata = array(
                    'user_id' => $user->user_id,
                    'uname' => $user->uname,
                    'authenticated' => TRUE
               );
               $this->session->set_userdata($userdata);
               redirect('dashboard');
            }
            else {
                $this->session->set_flashdata('msg','Invalid email or password');
                redirect('users/signin');
            }
        } 
    }
    public function signup()
    {
        $this->form_validation->set_rules('username','Username','required');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'required');
        $this->form_validation->set_rules('created_date','Created_date','required');
        $this->form_validation->set_rules('phone','Phone','required');

        if ($this->form_validation->run() == FALSE) {
            $this->load->view('signup');
        } else {
            $data = array(
                'uname' => $this->input->post('username'),
                'email' => $this->input->post('email'),
                'password' => $this->input->post('password'),
                'created_date' => $this->input->post('created_date'),
                'phone' => $this->input->post('phone')
            );
            $this->User_model->save($data);
            $this->session->set_flashdata('success', 'User added successfully!');
            redirect(base_url().'index.php/users/signup');
        }
    }
 
   
}
?>