<?php

use Restserver\Libraries\REST_Controller;

defined('BASEPATH') or exit('no direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';


class Users extends  REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("User_model");
    }

    public function index_get($id = 0)
    {
        if (!empty($id)) {

            $data = $this->User_model->Getuserbyid($id);
            if (empty($data)) {
                $this->response(array("status:" => "failed.", "message:" => "user not found."), REST_Controller::HTTP_NOT_FOUND);
            }
        } else {
            $data = $this->User_model->Allusers();
        }

        $this->response(array("status:" => "success.", "data :" => $data), REST_Controller::HTTP_OK);
    }

    public function index_post()
    {
        $input = $this->input->post();
        $status = $this->User_model->Adduser($input);
        if ($status) {
            $this->response(array("status:" => "success.", "message :" => "user is added successfully."), REST_Controller::HTTP_OK);
        } else {
            $this->response(array("status:" => "failed.", "message :" => "user is not added."), REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function index_put($id)
    {
        $input = $this->put();
        $status = $this->User_model->Updateuser($id, $input);
        if ($status) {
            $this->response(array("status:" => "success.", "message :" => "user updated successfully."), REST_Controller::HTTP_OK);
        } else {
            $this->response(array("status:" => "failed.", "message :" => "user  not updated."), REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function index_delete($id)
    {
        if ($id) {
            $status = $this->User_model->Deleteuser($id);
            if ($status) {
                $this->response(array("status:" => "success.", "message :" => "user deleted successfully."), REST_Controller::HTTP_OK);
            } else {
                $this->response(array("status:" => "failed.", "message:" => "some problem occurd."), REST_Controller::HTTP_NOT_FOUND);
            }
        } else {
            $this->response(array("status:" => "failed.", "message:" => "user not found."), REST_Controller::HTTP_NOT_FOUND);
        }
    }
}
