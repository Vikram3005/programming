<?php

defined('BASEPATH') or exit('no direct script access allowed');

class User_model extends CI_Model
{
    public  function __construct()
    {
        parent::__construct();
    }

    public function Allusers()
    {
        $users = $this->db->get("users")->result();
        return $users;
    }
    public function Getuserbyid($id)
    {
        $user = $this->db->get_where("users", ["user_id" => $id])->row_array();
        return $user;
    }

    public function Adduser($data)
    {
        if (!empty($data)) {
            $this->db->insert('users', $data);
            return true;
        } else {
            return false;
        }
    }
    public function Updateuser($id, $input)
    {
        if (!empty($id)) {
            $this->db->update('users', $input, ['user_id' => $id]);
            return true;
        } else {
            return false;
        }
    }
    public function Deleteuser($id)
    {
          $delete =  $this->db->delete('users',['user_id' => $id]);
            return $delete ? true:false;
    }
}
